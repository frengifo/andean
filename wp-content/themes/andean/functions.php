<?php 
	
//Desarrollador: Fernando Rengifo >.<
function custom_post_type() {
	/*register_post_type(
		'revista', array(
			'labels' => array('name' => 'Revista', 'singular_name' => 'Revista'),
			'public' => TRUE,
			'rewrite' => array( 'slug' => 'revista' ),
			'has_archive' => TRUE,
			'taxonomies' => array('category'),
			'supports' => array('title','editor','author','thumbnail','excerpt','revisions'),
			)
		);*/
	/*register_post_type(
		'lookbook', array(
			'labels' => array('name' => 'Lookbook', 'singular_name' => 'Lookbook'),
			'public' => TRUE,
			//'rewrite' => array( 'slug' => 'lookbook' ),
			'has_archive' => TRUE,
			'taxonomies' => array('category'),
			'supports' => array('title','editor','author','thumbnail','excerpt','revisions'),
			)
		);*/
	
}
add_action( 'init', 'custom_post_type' );

add_theme_support( 'post-thumbnails', array( 'revista', 'lookbook' ) );

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

function limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}

// para peticiones de usuarios que no están logueados
add_action('wp_ajax_nopriv_get_next_posts', 'ajax_get_next_posts');
// probablemente también vas a querer que los usuarios logueados puedan hacer lo mismo
add_action('wp_ajax_get_next_posts', 'ajax_get_next_posts');
function ajax_get_next_posts(){
	// usamos absint() para sanitizar el valor y recibir un int
	$offset     = absint( $_REQUEST['offset'] ) + 1;	
	$term_id = $_GET['category'];
	$next_posts = new WP_Query(array(
		'offset'      => $offset,
		'post_type'   => 'revista',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'destinos',
				'field' => 'term_id',
				'terms' => $term_id
			),
		), 
	));
	$i=0; 
	while ( $next_posts->have_posts() ) : $next_posts->the_post(); ?>

		<div class="col-md-4 col-sm-4 col-xs-12 hentry">
			<figure>
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
				<figcaption><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></figcaption>
			</figure>
			<p><?php echo $tag_show; ?> // <time datetime="<?php the_time(); ?>"><?php echo get_the_date('F j, Y'); ?></time></p>
			<article><?php echo limit_words(get_the_excerpt(), 26); ?></article>
		</div>
		


		<?php
		
		$i++;
	endwhile;
	//echo json_encode( array() );
	wp_die();
}

add_action('wp_ajax_nopriv_register_contact', 'ajax_register_contact');
add_action('wp_ajax_register_contact', 'ajax_register_contact');

function ajax_register_contact() {
	global $wpdb;
	$wpdb->insert( 
		'contactos', 
		array( 
			'name' => $_GET['name'], 
			'email' => $_GET['email'] ,
			'telephone' => $_GET['telephone'] ,
			'message' => $_GET['message'] ,
		)
	);

	wp_die();

}

add_action('wp_ajax_nopriv_register_suscriptor', 'ajax_register_suscriptor');
add_action('wp_ajax_register_suscriptor', 'ajax_register_suscriptor');

function ajax_register_suscriptor() {
	global $wpdb;
	$email = $wpdb->get_results( "SELECT id FROM suscriptores where email = '".$_GET['email']."' limit 0,1" );
	$response = array('success' => true, 'message' => 'Gracias por suscribirte.' );
	if ( !isset( $email[0] )) {
		$wpdb->insert( 
			'suscriptores', 
			array( 
				'name' => $_GET['name'], 
				'email' => $_GET['email'] ,
			)
		);
	}else{
		$response['message'] = 'Usted ya es un suscriptor nuestro.';
		$response['success'] = false;
	}
	
	echo json_encode( $response );

	wp_die();

}

 ?>