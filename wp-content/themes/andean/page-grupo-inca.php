<?php get_header(); ?>
<section class="breadcrumbs bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li>
                <a href="#" title="Envía tu prenda">Grupo Inca</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <div class="contact">
      
      <div class="container">
        <div class="row">
          <section class="col-md-12">
            <article>
              <?php while ( have_posts() ) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile;  ?>
            </article>

          </section>
        </div>
      </div>
    </div>
<?php get_footer(); ?>