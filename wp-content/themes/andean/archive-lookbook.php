<?php get_header( ); ?>
<section class="">
	
</section>
<section class="lookbook">
	<div class="heading">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Lookbook</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				
				<article>
					<p>
					
						En Andean creemos en momentos extraordinarios inspirados en lugares y en costumbres de nuestro país. Andean es ver la realidad con otros ojos y descubrir la belleza de lo espontáneo, de lo libre.

					</p>

					<p>
						
						Nos sentimos orgullosos de nuestro origen peruano y somos parte su milenaria tradición textil. En nuestras prendas ricas en colores y texturas unimos las bondades de las fibras de los Andes, como son la frescura, el aislamiento térmico y la suavidad, con fibras sintéticas especialmente diseñadas para brindar durabilidad y resistencia que sumadas brindan una extraordinaria experiencia de uso.

					</p>
				</article>

				<!--<p>
					Información de la colección
				</p>

				<p>
					
					Este año Andean recorre lugares inéditos del Perú inspirándose en sus paisajes, arquitectura, colores y textura, dando lugar a ocho mini colecciones que forman parte de la temporada otoño/invierno 2016. Cada uno de estos conceptos recibe un nombre Quechua con un significado relacionado a su fuente de inspiración.

				</p>-->
			</div>
			<style type="text/css">
				.lookbook .box img {
				    width: 100%;
				    max-height: 305px;
				}
				.lookbook .box {
				    
				    width: 45%;
				    margin: 1.5em 1.5em;
				  
				}
				@media (max-width: 1000px){


					.lookbook .box {
					    width: 40%;
					    min-width: 300px;
					}
					.lookbook .box a{
						font-size: 3em !important;

					}

				}
			</style>
			<?php $i=0; ?>
			<?php while ( have_posts() ) : the_post(); ?>
				
				<div class="box">
					<figure class="">
						<a href="<?php the_permalink(); ?>" class="title-lookbook" style=""><?php the_title(); ?></a>
						<a href="<?php the_permalink(); ?>" class="mask"> <?php the_post_thumbnail('full'); ?> </a>
					</figure>
				</div>
				<?php if ( $i == 0): ?>
					<div class="clear"></div>
				<?php endif ?>

				<?php $i++; ?>

			<?php endwhile; ?>

			<p>&nbsp;</p>

		</div>
	</div>
</section>	

<?php get_footer( ); ?>