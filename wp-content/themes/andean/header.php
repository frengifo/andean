<?php
/**
 * Desarrollador: frengifo.nando@gmail.com
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="/favicon.png" />
    <?php  if ( get_post_type() != "revista"){ ?>
      <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main.css" rel="stylesheet" type="text/css">  
    <?php }else{?> 
      <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/revista.css" rel="stylesheet" type="text/css">  
    <?php } ?>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <title><?php wp_title( '|', true, 'right' ); ?> Andean</title>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.5&appId=565727476923027";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- Place this tag in your head or just before your close body tag. -->
    <script src="https://apis.google.com/js/platform.js" async defer>
    {lang: 'es-419'}
    </script>
    <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/vendor/jquery-1.12.0.min.js"></script>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/vendor/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/vendor/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
</head>

<body <?php body_class(); ?>>
    

    <?php if ( get_post_type() != "revista"){ ?>

      <?php get_template_part( 'content', 'header' );  ?>

    <?php }else{?> 

      <?php get_template_part( 'content', 'header-revista' );  ?> 

    <?php } ?>
      
      

    
      
    