<?php get_header( ); ?>
<section class="">
	
</section>
<section class="detail">
	<div class="container">
		<div class="row">
			<div class="col-md-8 single-post">
				<?php  $id_single_post = 0; ?>
				<?php while ( have_posts() ) : the_post(); ?>


					<?php  $id_single_post = get_the_id(); ?>
					<?php $tags = get_tags(); ?>
					
					<?php shuffle( $tags ) ?>
					<?php $tag_show = $tags[0]->name; ?>
					
					<h1><?php the_title(); ?></h1>
					<p><?php echo $tag_show; ?> // <time datetime="<?php the_time(); ?>"><?php the_date('F j, Y'); ?></time></p>
					<article>
						
						<?php the_content(); ?>

					</article>
					<section class="share-buttons">
						<div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="RevistaAndean" data-show-count="true" data-lang="es">Twittear</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						<!-- Place this tag where you want the +1 button to render. -->
    					<div class="g-plusone" data-size="medium" data-href="<?php the_permalink(); ?>"></div>
					</section>

				<?php endwhile; ?>
			</div>
			<div class="col-md-4 aside-latest-post">
				<aside class="row">
					<div class="col-md-12"><h2>Últimos artículos</h2></div>
					<?php 

					$args = array( 'numberposts' => '3', 'post_type'=>'revista', 'exclude' => array( $id_single_post ) );
					$recent_posts = wp_get_recent_posts( $args );
					foreach( $recent_posts as $recent ){ ?>

						
						<article class="col-md-12 col-sm-6 col-xs-12">
							<a href="<?php echo get_permalink($recent["ID"]); ?>"><?php echo $recent["post_title"]; ?></a>
							<p> <time> <?php the_date( $recent["post_date"]); ?> Marzo 25, 2016</time> </p>

							<figure>
								<?php 	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($recent["ID"]), 'medium' );
										$url = $thumb['0']; ?>
								<a href="<?php echo get_permalink($recent["ID"]); ?>">
									<img src="<?php echo $url; ?>" width="100%">
								</a>
							</figure>
						</article>
					<?php } ?>


					
				</aside>
			</div>
		</div>
	</div>
</section>	

<?php get_footer( ); ?>