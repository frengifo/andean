<?php 



/**



 * Template Name: Inicio



 *



 */


?>


 <?php get_header(); ?>






	<div class="section slider">



        <section>



          	<ul>







	          	<?php 







		        $args = array( 'post_type' => 'inicio-andean', 'category_name' => 'slider' , 'posts_per_page' => 10 );



		        $loop = new WP_Query( $args );



		        while ( $loop->have_posts() ) : $loop->the_post();  ?>







		          	<li><img src="<?php the_field('imagen'); ?>" alt="<?php the_title(); ?>"></li>



	            	



		        <?php endwhile; ?>



            



          	</ul>



        </section>



    </div>



    <div class="section offers" style="display:none;">



      <div class="container">



        <div class="row">







	      	<?php 







	        $args = array( 'post_type' => 'inicio-andean', 'category_name' => 'descuentos' , 'posts_per_page' => 1 );



	        $loop = new WP_Query( $args );



	        while ( $loop->have_posts() ) : $loop->the_post();  ?>



	          <div class="col-md-6 col-xs-12 box">



	            <a href="<?php the_field('url'); ?>"><img src="<?php the_field('imagen'); ?>" alt="Ofertas 50%"></a>



	          </div>                    



	        <?php endwhile; ?>







	        <?php 







	        $args = array( 'post_type' => 'inicio-andean', 'category_name' => 'gift' , 'posts_per_page' => 1 );



	        $loop = new WP_Query( $args );



	        while ( $loop->have_posts() ) : $loop->the_post();  ?>







	        	<div class="col-md-3 col-xs-6 box">



			        <a href="<?php the_field('url'); ?>"><img src="<?php the_field('imagen'); ?>" alt="Gift"> <span><?php the_title(); ?> <small><?php the_field('descuento'); ?></small></span></a>



			    </div>







	       	<?php endwhile; ?>



	          



	        <?php 







	        $args = array( 'post_type' => 'inicio-andean', 'category_name' => 'ofertas' , 'posts_per_page' => 1 );



	        $loop = new WP_Query( $args );



	        while ( $loop->have_posts() ) : $loop->the_post();  ?>







	        	<div class="col-md-3 col-xs-6 box">



			        <a href="<?php the_field('url'); ?>"><img src="<?php the_field('imagen'); ?>" alt="Gift"> <span><?php the_title(); ?></span></a>



			    </div>







	       	<?php endwhile; ?>











        </div>



      </div>



    </div>



    <div class="section heading text-center">



      <div class="container">



        <div class="row">



          <h2 class="title"><span>DESTACADOS</span></h2>



        </div>



      </div>



    </div>



    <div class="section featured other-post">



      <div class="container">



        <div class="row">







        <?php 







	        $args = array( 'post_type' => 'inicio-andean', 'category_name' => 'destacados' , 'posts_per_page' => 2 );



	        $loop = new WP_Query( $args );



          $i=0;



	        while ( $loop->have_posts() ) : $loop->the_post();  ?>







            <?php $i++; ?>







          	<div class="col-md-6 col-sm-6 box grid">



            



	            <figure class="effect-lily">



	              <img src="<?php the_field('imagen'); ?>" alt="<?php the_title(); ?>">



	              <figcaption style="<?php echo $i % 2 == 0 ? "opacity:0;":""; ?>">



	                <div>



	                  <h2><a href="javascript:;"><?php the_title(); ?></a></h2>



	                  <article>



	                  	<?php the_content(); ?>



	                  </article>



	                </div>



	                <a href="<?php the_field('url'); ?>">View more</a>



	              </figcaption>     



	            </figure>







          	</div>







        <?php endwhile; ?>







        </div>



      </div>



    </div>





    <div class="section heading text-center">



      <div class="container">



        <div class="row">



          <h2 class="title"><span>NOVEDADES</span></h2>



        </div>



      </div>



    </div>



    <div class="section news">



      <div class="container">



        <div class="row slick-box" style="overflow:hidden">



          <div class="pull-left box">



            



            <figure>



              <a href="<?php site_url()?>lookbook/kaynin/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/kaynin.png" alt="Kaynin"></a>



              <figcaption>



                <a href="<?php site_url()?>lookbook/kaynin/">KAYNIN</a>



              </figcaption>



              <!--<ul class="pull-left">



                <li class="pull-left">



                  <a href="#" class="color-gray">GRAY</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-green">GREEN</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-red">RED</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-blue">BLUE</a>



                </li>



              </ul>-->



            </figure>







          </div>



          <div class="pull-left box">



            



            <figure>



              <a href="<?php site_url()?>lookbook/pirqa/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/pirqa.png" alt="Pirqa"></a>



              <figcaption>



                <a href="<?php site_url()?>lookbook/pirqa/">PIRQA</a>



              </figcaption>



              <!--<ul class="pull-left">



                <li class="pull-left">



                  <a href="#" class="color-gray">GRAY</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-green">GREEN</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-red">RED</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-blue">BLUE</a>



                </li>



              </ul>-->



            </figure>







          </div>



          <div class="pull-left box">



            



            <figure>



              <a href="<?php site_url()?>lookbook/rumi/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/rumi.png" alt="Rumi"></a>



              <figcaption>



                <a href="<?php site_url()?>lookbook/rumi/">RUMI</a>



              </figcaption>



              <!--<ul class="pull-left">



                <li class="pull-left">



                  <a href="#" class="color-gray">GRAY</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-green">GREEN</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-red">RED</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-blue">BLUE</a>



                </li>



              </ul>-->



            </figure>







          </div>



          <div class="pull-left box">



            



            <figure>



              <a href="<?php site_url()?>lookbook/sequey/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/sequey.png" alt="Sequey"></a>



              <figcaption>



                <a href="<?php site_url()?>lookbook/sequey/">S´EQUEY</a>



              </figcaption>



              <!--<ul class="pull-left">



                <li class="pull-left">



                  <a href="#" class="color-gray">GRAY</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-green">GREEN</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-red">RED</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-blue">BLUE</a>



                </li>



              </ul>-->



            </figure>







          </div>



          <div class="pull-left box">



            



            <figure>



              <a href="<?php site_url()?>lookbook/phusa/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/phusa.png" alt="Phusa"></a>



              <figcaption>



                <a href="<?php site_url()?>lookbook/phusa/">PHUSA</a>



              </figcaption>



              <!--<ul class="pull-left">



                <li class="pull-left">



                  <a href="#" class="color-gray">GRAY</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-green">GREEN</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-red">RED</a>



                </li>



                <li class="pull-left">



                  <a href="#" class="color-blue">BLUE</a>



                </li>



              </ul>-->



            </figure>

            <style type="text/css">

                .news figcaption, .news{

                  text-align: center;

                }

                .news a{

                  display:  inline-block;

                }

            </style>





          </div>



          <!--<div class="pull-left box">



            



            <figure>



              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/.png" alt="Titulo"></a>



              <figcaption>



                <a href="#" title="">Waves High Neck Pullover</a>



              </figcaption>





            </figure>







          </div>-->



          <!--<div class="pull-left box">



            



            <figure>



              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-4.png" alt="Titulo"></a>



              <figcaption>



                <a href="#" title="">Waves High Neck Pullover</a>



              </figcaption>



              



            </figure>







          </div>-->



        </div>



      </div>



    </div>











<?php get_footer(); ?>