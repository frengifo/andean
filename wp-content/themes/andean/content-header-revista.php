<div class="section top-menu <?php echo get_post_type(); ?>">
      <div class="container">
        <div class="row">
         
          <form class="search-form col-md-2 col-sm-3 col-xs-12 text-right" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            
            <input type="search" value="<?php echo get_search_query(); ?>" placeholder="Buscar" name="s" id="s" />
          </form>
        </div>
      </div>
    </div>
    <section class="logo">
      <div class="container">
        <div class="row"></div>
          <h2 class="pull-left"><a href="<?php echo site_url(); ?>/revista" >Destinos</a></h2>
          <a href="<?php echo site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-andean.png" alt="Andean"></a>
        </div>
      </div>
    </section>
    <section class="category-menu main-navigation">
      <a href="javascript:;" class="menu-dropdown"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/menu.png"> </a>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            
            <?php 

                wp_nav_menu( array(
                  'menu' => 'menu-categorias'
                ) );

             ?>

          </div>
        </div>
      </div>
    </section>