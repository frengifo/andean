<?php get_header(); ?>
<section class="breadcrumbs bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li>
                <a href="#" title="Envía tu prenda">Tiendas</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <div class="contact">
      <style type="text/css">
      .contact{
        overflow-x: hidden; 
      }
      .contact p{
        font-size:  .9em;
        line-height: .8em;

      }

      .contact article{
        padding-top:  1.5em
      }
      .contact .cities{
        margin: 0 0 2em;    font-size: .55em;
      
      }
      .cities a{
        color:#333;
      }

      </style>
      <div class="">
        <div class="row">
          <section class="col-md-12">
          <script src="http://maps.google.com/maps/api/js?sensor=false"></script>

          

          <script type="text/javascript">
            function moveCenter(latitud, longitud, index){
              var center = new google.maps.LatLng(latitud, longitud);
                // using global variable:
                map.panTo(center);
                google.maps.event.trigger(markers[index], 'click');

            }
              // When the window has finished loading create our google map below



              google.maps.event.addDomListener(window, 'load', init);
              var locations = [
                 
                    <?php 

                    $args = array( 'post_type' => 'tienda', 'posts_per_page' => 10 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();  ?>
                      
                      <?php $html = json_encode( "<h3>".get_the_title()."</h3>", JSON_HEX_TAG ); ?>

                      [<?php  echo $html; ?>, <?php the_field('latitud'); ?>, <?php the_field('longitud'); ?>],
                                          
                    <?php endwhile; ?>
              ];

              /*var locations = [
                ['Trujillo', 40.6700, -73.9400]
                
              ];*/
              var map;
              var markers = [];
              function init() {
                  // Basic options for a simple Google Map
                  // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                  var mapOptions = {
                      // How zoomed in you want the map to start at (always required)
                      zoom: 12,
                      scrollwheel: false,
                      // The latitude and longitude to center the map (always required)
                      center: new google.maps.LatLng(-12.0678926, -77.0380931), // New York

                      // How you would like to style the map. 
                      // This is where you would paste any style found on Snazzy Maps.
                      //styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f8f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#42b8e0"},{"visibility":"on"}]}],
                      //disableDefaultUI: true,
                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                  zoomControl: true
                  };

                  var mapElement = document.getElementById('map');


                  var infowindow = new google.maps.InfoWindow();
                  // Create the Google Map using our element and options defined above
                  map = new google.maps.Map(mapElement, mapOptions);
                  //var marker, i;
                  for (var i = 0; i < locations.length; i++) {
                          // Let's also add a marker while we're at it
                      var marker = new google.maps.Marker({
                          position: new google.maps.LatLng(locations[i][1],locations[i][2] ),
                          map: map,
                          icon:'<?php echo esc_url( get_template_directory_uri() ); ?>/img/map-marker-icon.png'
                      });

                      google.maps.event.addListener(marker, 'click', (function(marker, i) {
                          return function() {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                          }
                      })(marker, i));
                      markers.push(marker);
                  };
              }

             
          </script>
            <article>
             <div class="container"><p class="cities">
              <a href="javascript:;" onclick="setCenterLima(this)">Lima</a> | <a href="javascript:;" onclick="setCenterArequipa(this)">Arequipa</a></p></div>
              <?php while ( have_posts() ) : the_post(); ?>

                <?php the_content(); ?>

              <?php endwhile;  ?>
              <div id="map" style="height:780px"></div>
            </article>

          </section>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function setCenterLima(ele){
      map.setCenter(new google.maps.LatLng(-12.085712, -76.9767395));
       $(".cities a").removeClass("active");
       $(ele).addClass("active");
    }
    function setCenterArequipa(ele){
      map.setCenter(new google.maps.LatLng(-16.4040904, -71.5532299));
      $(".cities a").removeClass("active");
      $(ele).addClass("active");
    }
    </script>
<?php get_footer(); ?>