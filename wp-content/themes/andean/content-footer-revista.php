	 <?php get_template_part( 'content', 'newsletter' );  ?>
    <footer class="section footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-xs-12 text-center">
            <a href="#">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-andean-white.png">
            </a>
          </div>
          <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <ul>
                  <li>
                    <a href="<?php echo site_url('/tiendas'); ?>">Tiendas</a>
                  </li>
                  <li>
                    <a href="#proteccion" class="various">Protección de datos</a>
                  </li>
                  
                </ul>
              </div>
              <div class="col-md-5 col-sm-4 col-xs-12">
                <ul>
                  <li>
                    <a href="<?php echo site_url('/contacto'); ?>">Contacto</a>
                  </li>
                  <li>
                    <a href="<?php echo site_url('/grupo-inca'); ?>">Grupo Inca</a>
                  </li>
                </ul>
              </div>
              <style type="text/css">

                .footer a.pin{
                  border: 1px solid #464646;
                  width: 32px;
                  height: 34px;
                  padding: .7em .5em;
                  display: inline-block;
                  text-align: center;
                  margin: 0 8px 0 0;
                }
                .footer a.fb:hover, .footer a.tw:hover, .footer a.insta:hover, .footer a.pin:hover {
                    color: #2d2d2f;
                    background-color: #fff;
                }
                
              </style>
              <div class="col-md-3 col-sm-4 col-xs-12" style="padding-left:0;">
                <p>Síguenos en:</p>
                <br>
                <a href="#" class="fb"><i class="fa fa-facebook fa-lg"></i></a>
                <a href="#" class="tw"><i class="fa fa-twitter fa-lg"></i></a>
                <a href="#" class="insta"><i class="fa fa-instagram fa-lg"></i></a>
                <a href="" class="pin"> <i class="fa fa-pinterest-p"></i> </a>
              </div>
            </div>
          </div>
        </div>
        
      </div>

    </footer>
    <section class="copy">
      <div class="container">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/mzn.png" class="pull-right" alt="Development by mimanzana">
        <p>COPYRIGHT 2016. Todos los derechos reservados</p>
      </div>    
    </section>
     <div id="proteccion" style="display:none;">
      <?php 
        $post_proteccion_datos = get_post( 111 ); 
        echo $post_proteccion_datos->post_content;
      ?>
    </div>
  </body>

  
  <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/plugins.js"></script>
  <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>
  <?php wp_footer(); ?>
  <script type="text/javascript">
    var url_site = '<?php echo site_url(); ?>';
  </script>
</html>