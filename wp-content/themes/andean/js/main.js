jQuery(document).ready(function($){

	/**JAVASCRIPT**/
	$('.slider > section').unslider({
		nav: false,
		arrows: {
			//  Unslider default behaviour
			prev: '<a class="unslider-arrow prev">&#10094;</a>',
			next: '<a class="unslider-arrow next">&#10095;</a>',
		}
	});

	$(".slick-box").slick({
	  	dots: false,
	  	infinite: false,
	  	speed: 300,
	  	slidesToShow: 4,
	  	slidesToScroll: 4,
			responsive: [
			    {
			      	breakpoint: 1024,
			      	settings: {
				        slidesToShow: 3,
				        slidesToScroll: 3,
				        infinite: true,
					        dots: true
					   	}
			    },
			    {
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
			    },
			    {
			      	breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
			    }
			]
	});

	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

	$(".menu-dropdown").on("click", function(){
		$(this).next().toggleClass("open");
		$(this).next().slideToggle();
	})

	
	$('.view-more').on('click', function(){
		loadPost();
		$(this).fadeOut();
	});
	$(".gallery").unslider();

	$('#contactForm').submit(function(e) { 
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {
           //alert("enviar!");

           jQuery.get( url_site + '/wp-admin/admin-ajax.php', {

				action: 'register_contact',
				name: $(this).find("input[name='name']").val(),
				email: $(this).find("input[name='email']").val(),
				telephone: $(this).find("input[name='tel']").val(),
				message: $(this).find("textarea").val()

			}, function(data){
					
				$("#contactForm .msg-success").fadeIn();

			}, 'json');


        }
    });

	$('#formSuscriptor').submit(function(e) { 
        e.preventDefault();

        if ( $( '#name' ).parsley( 'isFieldValid' ) && $( '#email' ).parsley( 'isFieldValid' ) ) {
            
            //alert("validar proteccion!");

            if ( $("#check-proteccion").is(":checked") ) {

	            //alert("guardar datos");
	            //$("#check-proteccion").
	            jQuery.get( url_site + '/wp-admin/admin-ajax.php', {

					action: 'register_suscriptor',
					name: $(this).find("input[name='name']").val(),
					email: $(this).find("input[name='email']").val(),
					

				}, function(data){
					
					if ( data.success ) {

						$("#formSuscriptor").hide();
						$(".newsletter .msg-success").fadeIn();
						$(".newsletter .msg-success").html( data.message );	
						$(".newsletter .msg-success").removeClass("duplicate");

					}else{

						$(".newsletter .msg-success").fadeIn();
						$(".newsletter .msg-success").html( data.message );
						$(".newsletter .msg-success").addClass( "duplicate" );

					}

					$(".newsletter").find("input").val("");
					

				}, 'json');

			}else{
				$(".btn-suscriptor").trigger("click");
			}

        }

        $("#check-proteccion").on("change", function(){
        	if ($(this).is(":checked")) {
        		$.fancybox.close();
        		$(".btn-suscribe").trigger("click");
        	};
        })

    });


})

function loadPost(){
	// la variable ajaxurl debe estar definida y apuntar a wp-admin/admin-ajax.php
	// en la data enviada con la petición, el parámetro "action" debe coincidir con la detección de la acción en PHP

	var postTemplate = function( post ){
		return '<article class="hentry"><a href="#post-'+ post.ID +'">'+ post.post_title +'</a></article>';
	}


	jQuery.get( url_site + '/wp-admin/admin-ajax.php', {
		action: 'get_next_posts',
		category: cat_id,
		offset: jQuery('.latest-post').find('.hentry').length
	}, function(data){
		
		jQuery('.latest-post').append( data );

	}, 'html');
}