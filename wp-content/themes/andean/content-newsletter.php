<div class="section newsletter">
      <div class="container">
        <div class="row">
         <hr>
          <div class="col-md-4">
            <h3>ÍNSCRIBETE A NUESTRO BOLETÍN MENSUAL</h3>
            <p><a href="#suscriptor-terminos" class="various">Ver términos y condiciones</a></p>
          </div>
          <div class="col-md-8">
            <div class="col-md-12">
              <div class="row">
                <form action="#" method="post" id="formSuscriptor" data-parsley-validate>
                  <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="row">
                      <div class="col-md-6  col-sm-6 col-xs-12">
                        <input type="text" data-parsley-pattern="^[A-Za-z ]+$" name="name" placeholder="Nombre" id="name" required>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" name="email" placeholder="Ingresa tu e-mail" id="email" required>
                      </div>
                      <div class="msg hidden">Mensaje </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                    <button type="submit" class="btn-suscribe">Inscribirme</button>
                  </div>
                  <a href="#suscriptor-proteccion" class="various btn-suscriptor">check proteccion</a>
                </form>
                <div class="msg-success">Gracias por suscribirte</div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div id="suscriptor-proteccion">
    <h2>Protección de datos</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p>
      <label>
        <input type="checkbox" name="check-proteccion" id="check-proteccion" required> Protección de datos
      </label>
    </p>
    
  </div>

  <div id="suscriptor-terminos">
    <h2>Términos y condiciones</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    
    
  </div>