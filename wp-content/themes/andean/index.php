<?php get_header(); ?>


	<div class="section slider">
        <section>
          <ul>
            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/default.png" alt="Andean Descuentos"></li>
            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/default.png" alt="Andean Descuentos"></li>
          </ul>
        </section>
    </div>
    <div class="section offers">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12 box">
            <a href="<?php echo site_url('/lookbook/kaynin/' ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/descuento.png" alt="Ofertas 50%"></a>
          </div>
          <div class="col-md-3 col-xs-6 box">
            <a href="#"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/gift.png" alt="Gift"> <span>GIFTS <small>80%</small></span></a>
          </div>
          <div class="col-md-3 col-xs-6  box">
            <a href="#"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ofertas.png" alt="Ofertas"> <span>OFERTAS</span> </a>
          </div>
        </div>
      </div>
    </div>
    <div class="section heading text-center">
      <div class="container">
        <div class="row">
          <h2 class="title"><span>DESTACADOS</span></h2>
        </div>
      </div>
    </div>
    <div class="section featured other-post">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 box grid">
            
            <figure class="effect-lily">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/destacado-1.png" alt="Titulo destacado 1">
              <figcaption>
                <div>
                  <h2><a href="javascript:;">Titulo</a></h2>
                  <p>Loremp ipsum dolo [..]</p>
                </div>
                <a href="javascript:;">View more</a>
              </figcaption>     
            </figure>

          </div>
          <div class="col-md-6 col-sm-6 box grid">
            
            <figure class="effect-lily">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/destacado-2.png" alt="Titulo destacado 2">
              <figcaption>
                <div>
                  <h2><a href="javascript:;">Titulo</a></h2>
                  <p>Loremp ipsum dolo [..]</p>
                </div>
                <a href="javascript:;">View more</a>
              </figcaption>     
            </figure>
          </div>
        </div>
      </div>
    </div>
    <p>&nbsp;</p>
    <div class="section heading text-center" style="display:none">
      <div class="container">
        <div class="row">
          <h2 class="title"><span>NOVEDADES</span></h2>
        </div>
      </div>
    </div>
    <div class="section news" style="display:none">
      <div class="container">
        <div class="row slick-box" style="overflow:hidden">
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-1.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-2.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-3.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-4.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-2.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-3.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
          <div class="pull-left box">
            
            <figure>
              <a href="#" title=""><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/novedades-4.png" alt="Titulo"></a>
              <figcaption>
                <a href="#" title="">Waves High Neck Pullover</a>
              </figcaption>
              <ul class="pull-left">
                <li class="pull-left">
                  <a href="#" class="color-gray">GRAY</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-green">GREEN</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-red">RED</a>
                </li>
                <li class="pull-left">
                  <a href="#" class="color-blue">BLUE</a>
                </li>
              </ul>
            </figure>

          </div>
        </div>
      </div>
    </div>


<?php get_footer(); ?>