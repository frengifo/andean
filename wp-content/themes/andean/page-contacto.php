<?php get_header(); ?>
<section class="breadcrumbs bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li>
                <a href="#" title="Envía tu prenda">Contacto</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <div class="ubicacion">
      
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-12">
            <form action="#" method="post" id="contactForm" data-parsley-validate>
              <h3>Nombre</h3>
              <input type="text" name="name" required>
              <h3>Correo electrónico</h3>
              <input type="email" name="email" required>
              <h3>Teléfono</h3>
              <input type="text" name="tel" required>
              <h3>Mensaje</h3>
              <textarea name="message" required></textarea>
              <label class="protection"> <input type="checkbox" name="proteccion" required> 
                <a href="#proteccion" class="various">Protección de datos </a>
              </label>
              <button type="submit">Enviar</button>

              <p>&nbsp;</p>
              <div class="msg-success">Gracias por escribirnos</div>
            </form>
          </div>
          <div class="col-md-6 col-md-12">
            <div class="row">
              
              <?php 

                    $args = array( 'post_type' => 'tienda', 'posts_per_page' => 10 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();  ?>

                    <div class="col-md-6 col-sm-6 col-xs-6  item">
                      <h4><?php the_title(); ?></h4>
                      <div class="text">
                        <?php the_field('descripcion'); ?>
                      </div>
                    </div>

               <?php endwhile; ?>
            </div>
          </div>
        </div>
      </div>
      <div id="proteccion"></div>
    </div>
  <?php get_footer(); ?>