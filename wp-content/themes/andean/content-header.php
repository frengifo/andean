<div class="section top-menu">
  <div class="container">
    <div class="row">
      <ul class="col-md-10 col-sm-10 col-sm-9 col-xs-12">
        <li>
          <a href="#" class="heart">Whishlist</a>
        </li>
        <li>
          <a href="<?php echo site_url('/contacto'); ?>" class="mail">Contacto</a>
        </li>
        <!--<li>
          <a href="#"  class="es">ES</a>
        </li>
        <li>
          <a href="#" class="en">EN</a>
        </li>-->
      </ul>
      <form class="search-form col-md-2 col-sm-3 col-xs-12 text-right" >
        <input type="search" value="<?php echo get_search_query(); ?>" placeholder="Buscar" name="search" id="s" />
      </form>
    </div>
  </div>
</div>
<section class="logo">
  <a href="<?php echo site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-andean.png" alt="Andean"></a>
</section>
<div class="header">
  <a href="javascript:;" class="menu-dropdown"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/menu.png"> </a>
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <ul class="menu">
          <li class="active">
            <a href="<?php echo site_url('/lookbook'); ?>">LOOKBOOK</a>
          </li>
          <!--<li>
            <a href="#">HOMBRES</a>
          </li>
          <li>
            <a href="#">MUJERES</a>
          </li>-->
          <li>
            <a href="<?php echo site_url('/revista'); ?>">REVISTA<br></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>