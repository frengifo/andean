<?php
get_header();

$c = get_query_var('cat');
$id_cat = get_category( $c )->term_id;
//var_dump( $id_cat );
$cats_posts = new WP_Query(array('post_type'=>'any','cat'=> $id_cat  )); ?>
<?php 
	
	//var_dump($cats_posts);

 ?>

<section class="section-category-post">
	<section class="current-category">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2><?php single_cat_title(); ?> </h2>

					</div>
				</div>
			</div>
		</div> 	
	</section>
	<div class="container">
		<?php $i=0; ?>
		<script type="text/javascript"> var cat_id = <?php echo $id_cat; ?>; </script>
		<?php while ( $cats_posts->have_posts() ) : $cats_posts->the_post(); ?>
			
			<?php $tags = get_tags(); ?>
			<?php shuffle( $tags ) ?>
			<?php $tag_show = $tags[0]->name; ?>
			<?php if ( $i == 0): ?>
				
				<div class="row">
					<div class="col-md-12 full-category-post">
						<figure class="">
							<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('full'); ?> </a>
						</figure>
						<article>
							<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							<p> <?php echo $tag_show; ?> // <time datetime="<?php the_time(); ?>"><?php the_date('F j, Y'); ?></time></p>
							<section>
								<?php the_excerpt(); ?>
							</section>
							<div class="tags">
								<span><strong>TAGS:</strong> </span>
								<?php foreach ($tags as $key => $value): ?>
									<span><?php echo $value->name; ?></span> <span>//</span>
								<?php endforeach ?>
							</div>
						</article>

					</div>
				</div>


			<?php endif ?>
	
		<?php $i++; ?>

		<?php endwhile; ?>


		<?php $i=0; ?>
		<div class="row latest-post">
			<?php while ( $cats_posts->have_posts() ) : $cats_posts->the_post(); ?>
				
				<?php $tags = get_tags(); ?>
				<?php shuffle( $tags ) ?>
				<?php $tag_show = $tags[0]->name; ?>
			
				<?php if ( $i > 0 && $i < 4): ?>
					
					
						<div class="col-md-4 col-sm-4 col-xs-6 hentry">
							<figure>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
								<figcaption><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></figcaption>
							</figure>
							<p><?php echo $tag_show; ?> // <time datetime="<?php the_time(); ?>"><?php echo get_the_date('F j, Y'); ?></time></p>
							<article><?php echo limit_words(get_the_excerpt(), 26); ?></article>
						</div>
						<?php if ( $i % 3 == 0 ): ?>
							<div class="clear"></div>
						<?php endif ?>

				<?php endif ?>


			
			<?php $i++; ?>

			<?php endwhile; ?>

		</div>
		<a href="javascript:;" class="view-more"> Ver más</a>
	</div>
</section>
<?php

get_footer();
