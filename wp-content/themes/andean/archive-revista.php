<?php get_header(); ?>

	
	<?php 

	$args = array( 'post_type' => 'revista', 'posts_per_page' => 1, 'featured' => 'yes' );
	$loop = new WP_Query( $args );
	$id_featured = 0;
	while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<section class="full-post">
			
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-12">
					
					<figure>
						<img src="<?php the_post_thumbnail_url( 'full' ); ?> ">
						<figcaption>
							<div>
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<p><?php echo limit_words(get_the_excerpt(), 40) ?>[..]</p>
							</div>
						</figcaption>
					</figure>

				</div>	


			</div>
			
		</div>

	</section>
	  	<?php $id_featured = get_the_id(); ?>
	<?php endwhile;	 ?>


	<section class="other-post">
				
		<div class="container">
			
			<div class="row ">

				<?php 

				$args = array( 'post_type' => 'revista', 'posts_per_page' => 4 );
				$i=0;
				$j=0;
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php $i++; ?>

					<?php if ( $i > 1): ?>
						
						<?php if ( $i == 2){ ?>

							<div class="col-md-8 col-sm-12 col-xs-12 box medium grid">
								
								<!--<figure>
									<img src="<?php the_post_thumbnail_url( 'large' ); ?> ">
									<figcaption>
										<div class="">
											<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
											<p><?php echo limit_words(get_the_excerpt(), 40) ?></p>
										</div>
									</figcaption>
								</figure>-->

								<figure class="effect-lily">
									<img src="<?php the_post_thumbnail_url( 'large' ); ?>" alt="<?php the_title(); ?>"/>
									<figcaption>
										<div>
											<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
											<p><?php echo limit_words(get_the_excerpt(), 40) ?>[..]</p>
										</div>
										<a href="<?php the_permalink(); ?>">View more</a>
									</figcaption>			
								</figure>

							</div>
								

						<?php }else{ ?> 
							<?php $j++; ?>
								<div class="col-md-4 col-sm-6 col-xs-12 box grid">
									

											
											<figure class="effect-lily">
												<img src="<?php the_post_thumbnail_url( 'large' ); ?>" alt="<?php the_title(); ?>"/>
												<figcaption>
													<div>
														<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
														<p><?php echo limit_words(get_the_excerpt(), 30) ?>[..]</p>
													</div>
													<a href="<?php the_permalink(); ?>">View more</a>
												</figcaption>			
											</figure>

										

								</div>
								<?php if ( $j == 2) { ?>
									<div class="clear"></div>
								<?php } ?>
									

						<?php } ?>

					<?php endif ?>

						

				<?php endwhile;	 ?>

				<?php 

				$args = array( 'post_type' => 'revista', 'posts_per_page' => 10, 'post__not_in' => array( $id_featured ) );
				$i=0;
				
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php $i++; ?>
					<?php //var_dump( $loop ) ?>
					<?php if ( $i > 4): ?>
						
						
							
								<div class="col-md-4 col-sm-6 col-xs-12 box grid">
									
									
											
											<figure class="effect-lily">
												<img src="<?php the_post_thumbnail_url( 'medium' ); ?> ">
												<figcaption>
													<div>
														<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
														<p><?php echo limit_words(get_the_excerpt(), 30) ?>[..]</p>
													</div>
													<a href="<?php the_permalink(); ?>">View more</a>
												</figcaption>
											</figure>

										

								</div>
							

						

					<?php endif ?>

						

				<?php endwhile;	 ?>


			</div>

		</div>

	</section>



<?php get_footer(); ?>