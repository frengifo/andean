<?php get_header( ); ?>
<section class="">
	
</section>
<style type="text/css">
	.lookbook article img.size-full{
		width:  100%;
		margin-bottom:  1em;
	}
	.ug-lightbox .ug-textpanel-title, .ug-lightbox .ug-textpanel-description{
		opacity: 0;
		font-size: 0 !important;
	}
	.ug-lightbox .ug-lightbox-overlay{
		opacity: 0.9 !important;
	}
	.ug-slider-wrapper, .ug-item-wrapper img{
		border: 0 !important;
	}

	div.ug-thumb-wrapper:nth-child(-n+2) {
	    background: #ff0000;
	}
	
	@media(min-width:  1200px){
		.postid-75 div.ug-thumb-wrapper:last-child, .postid-73 div.ug-thumb-wrapper:last-child, .postid-71 div.ug-thumb-wrapper:last-child, .postid-62 div.ug-thumb-wrapper:last-child {
		   	left: 34.5% !important;
	    	top: 34.5% !important;
		}
		.postid-75 .ug-gallery-wrapper, .postid-73 .ug-gallery-wrapper, .postid-71 .ug-gallery-wrapper, .postid-62 .ug-gallery-wrapper{
			max-height: 535px;
		}
	}
</style>
<?php while ( have_posts() ) : the_post(); ?>
<section class="lookbook">
	<div class="heading">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				
				<article>
					<?php the_content(); ?>
				</article>
			</div>
			
		</div>
	</div>
</section>	
<?php endwhile; ?>
<?php get_footer( ); ?>